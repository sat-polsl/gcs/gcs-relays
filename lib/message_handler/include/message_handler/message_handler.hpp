#pragma once
#include <functional>
#include <map>
#include <memory>
#include <string_view>

#include "message_handler/message_handler_interface.hpp"

namespace gcs::socket {
class socket_base;
}

namespace gpio {
class gpio_interface;
}

namespace message_handler {
class message_handler final : public message_handler_interface {
 public:
  message_handler(std::weak_ptr<gcs::socket::socket_base> pub_socket,
                  std::weak_ptr<gpio::gpio_interface> rotator_gpio,
                  std::weak_ptr<gpio::gpio_interface> filter2m_gpio,
                  std::weak_ptr<gpio::gpio_interface> filter70cm_gpio);
  message_handler(const message_handler &) = delete;
  message_handler &operator=(const message_handler &) = delete;
  message_handler(message_handler &&) noexcept = delete;
  message_handler &operator=(message_handler &&) noexcept = delete;
  ~message_handler() = default;

  void handle(const gcs::socket::message &msg) override;

 private:
  void handle_set_relays(const std::string &msg);
  void handle_get_relays(const std::string &msg);

  std::weak_ptr<gcs::socket::socket_base> _pub_socket;
  std::weak_ptr<gpio::gpio_interface> _rotator_gpio;
  std::weak_ptr<gpio::gpio_interface> _filter2m_gpio;
  std::weak_ptr<gpio::gpio_interface> _filter70cm_gpio;
  std::map<std::string_view, std::function<void(const std::string &)>> _handlers;
};
}  // namespace message_handler
