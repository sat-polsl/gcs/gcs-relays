#include "gmock/gmock.h"
#include "gcs/socket/socket_base.hpp"

namespace gcs::socket::mock {
struct socket : public socket_base {
  socket() = default;
  ~socket() = default;

  MOCK_METHOD(std::optional<message>, receive, ());
  MOCK_METHOD(bool, send, (const message &));
  MOCK_METHOD(void, subscribe, (const std::string &topic));
  MOCK_METHOD(void, close, ());
  MOCK_METHOD(void, set_receive_timeout, (std::chrono::milliseconds timeout));
};
}  // namespace socket::mock
