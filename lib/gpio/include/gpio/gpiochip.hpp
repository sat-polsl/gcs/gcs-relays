#pragma once
#include <cstdint>

namespace gpio {
class gpiochip {
 public:
  static gpiochip &get();
  std::int32_t handle();

 private:
  gpiochip();
  gpiochip(const gpiochip &) = delete;
  gpiochip &operator=(const gpiochip &) = delete;
  gpiochip(gpiochip &&) noexcept = delete;
  gpiochip &operator=(gpiochip &&) noexcept = delete;
  ~gpiochip();

  std::int32_t _handle;
};
}  // namespace gpio