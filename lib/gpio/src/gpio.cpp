#include "gpio/gpio.hpp"

#include "gpio/gpiochip.hpp"
#include "lgpio.h"

gpio::gpio::gpio(std::int32_t pin, bool init_value) : _pin{pin} {
  lgGpioClaimOutput(gpiochip::get().handle(), LG_SET_PULL_UP | LG_SET_ACTIVE_LOW, _pin, init_value);
}

gpio::gpio::~gpio() { lgGpioFree(gpiochip::get().handle(), _pin); }

bool gpio::gpio::get_value() {
  std::int32_t value = lgGpioRead(gpiochip::get().handle(), _pin);
  return value;
}

void gpio::gpio::set_value(bool value) { lgGpioWrite(gpiochip::get().handle(), _pin, value); }
