#pragma once

#include <chrono>
#include <optional>
#include <string>

namespace gcs::socket {

struct message {
  std::string topic;
  std::string data;
};

class socket_base {
 public:
  virtual std::optional<message> receive() = 0;

  virtual bool send(const message& msg) = 0;

  virtual void subscribe(const std::string& topic) = 0;

  virtual void set_receive_timeout(std::chrono::milliseconds timeout) = 0;

  virtual void close() = 0;

  virtual ~socket_base() = default;
};
}  // namespace gcs::socket
