#include "gmock/gmock.h"
#include "gpio/gpio_interface.hpp"

namespace gpio::mock {
struct gpio : public gpio_interface {
  gpio() = default;
  ~gpio() = default;

  MOCK_METHOD(bool, get_value, ());
  MOCK_METHOD(void, set_value, (bool));
};
}  // namespace gpio::mock
