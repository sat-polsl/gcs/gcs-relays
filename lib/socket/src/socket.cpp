#include "gcs/socket/socket.hpp"

#include "gcs/socket/context.hpp"

namespace gcs::socket {
socket::socket(const std::string_view ip, const std::string_view port, zmq::socket_type type)
    : _socket(context(), type) {
  _socket.set(zmq::sockopt::linger, 0);
  _socket.connect("tcp://" + std::string(ip) + ":" + std::string(port));
}

socket::~socket() { _socket.close(); }

std::optional<message> socket::receive() {
  message result;
  zmq::message_t msg;

  try {
    auto recv_result = _socket.recv(msg);
    if (!(recv_result.has_value() && *recv_result != 0)) {
      return std::nullopt;
    }
    result.topic = msg.to_string();

    recv_result = _socket.recv(msg);
    if (!(recv_result.has_value() && *recv_result != 0)) {
      return std::nullopt;
    }

    result.data = msg.to_string();
  } catch (const std::exception&) {
    return std::nullopt;
  }

  return result;
}

bool socket::send(const message& msg) {
  auto result =
      _socket.send(zmq::const_buffer(msg.topic.data(), msg.topic.size()), zmq::send_flags::sndmore);

  if (!(result.has_value() && *result != 0)) {
    return false;
  }

  result = _socket.send(zmq::const_buffer(msg.data.data(), msg.data.size()));

  if (!(result.has_value() && *result != 0)) {
    return false;
  }

  return true;
}

void socket::subscribe(const std::string& topic) { _socket.set(zmq::sockopt::subscribe, topic); }

void socket::set_receive_timeout(std::chrono::milliseconds timeout) {
  _socket.set(zmq::sockopt::rcvtimeo, static_cast<int>(timeout.count()));
}

void socket::close() { _socket.close(); }

}  // namespace gcs::socket
