set(TARGET socket)

add_library(${TARGET} STATIC)

target_sources(${TARGET}
    PRIVATE
    src/socket.cpp
    src/context.cpp
    )

target_include_directories(${TARGET}
    PUBLIC
    include
    )

target_link_libraries(${TARGET}
    PUBLIC
    libzmq-static
    cppzmq-static
    )
