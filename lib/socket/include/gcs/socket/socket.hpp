#pragma once

#include "gcs/socket/socket_base.hpp"
#include "zmq.hpp"

namespace gcs::socket {
class socket final : public socket_base {
 public:
  socket(const std::string_view ip, const std::string_view port, zmq::socket_type type);

  socket(const socket&) = delete;

  socket& operator=(const socket&) = delete;

  socket(socket&&) noexcept = delete;

  socket& operator=(socket&&) noexcept = delete;

  ~socket();

  std::optional<message> receive() override;

  bool send(const message&) override;

  void subscribe(const std::string& topic) override;

  void set_receive_timeout(std::chrono::milliseconds timeout) override;

  void close();

 private:
  zmq::socket_t _socket;
};
}  // namespace gcs::socket
