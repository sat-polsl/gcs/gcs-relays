#!/bin/sh
exec gcs-relays --proxy ${PROXY_IP} --pub ${PUB_PORT} --sub ${SUB_PORT} --rotator ${ROTATOR_GPIO} --filter2m ${FILTER2M_GPIO} --filter70cm ${FILTER70CM_GPIO}
