#pragma once

namespace gcs::socket {
struct message;
}

namespace message_handler {
class message_handler_interface {
 public:
  virtual void handle(const gcs::socket::message &message) = 0;

 protected:
  ~message_handler_interface() = default;
};
}  // namespace message_handler
