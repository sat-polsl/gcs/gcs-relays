#include "gpio/gpiochip.hpp"

#include "lgpio.h"

namespace gpio {
gpiochip::gpiochip() { _handle = lgGpiochipOpen(0); }

gpiochip::~gpiochip() { lgGpiochipClose(_handle); }

gpiochip &gpiochip::get() {
  static gpiochip chip;
  return chip;
}
std::int32_t gpiochip::handle() { return _handle; }
}  // namespace gpio