#pragma once
#include <cstdint>

#include "gpio/gpio_interface.hpp"

namespace gpio {

class gpio final : public gpio_interface {
 public:
  gpio(std::int32_t pin, bool init_value);
  gpio(const gpio &) = delete;
  gpio &operator=(const gpio &) = delete;
  gpio(gpio &&) noexcept = delete;
  gpio &operator=(gpio &&) noexcept = delete;
  ~gpio();

  bool get_value() override;
  void set_value(bool value) override;

 private:
  std::int32_t _pin;
};
}  // namespace gpio