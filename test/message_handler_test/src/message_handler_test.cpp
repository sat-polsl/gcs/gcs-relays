#include "message_handler/message_handler.hpp"

#include <array>
#include <memory>

#include "gpio/mocks/gpio_mock.hpp"
#include "gtest/gtest.h"
#include "nlohmann/json.hpp"
#include "socket/mocks/socket_mock.hpp"

namespace {
using namespace testing;

using gpio_strict_mock = StrictMock<gpio::mock::gpio>;
using socket_strict_mock = StrictMock<gcs::socket::mock::socket>;

class message_handler_test : public Test {
 public:
  std::shared_ptr<gpio_strict_mock> _rotator{std::make_shared<gpio_strict_mock>()};
  std::shared_ptr<gpio_strict_mock> _filter2m{std::make_shared<gpio_strict_mock>()};
  std::shared_ptr<gpio_strict_mock> _filter70cm{std::make_shared<gpio_strict_mock>()};
  std::shared_ptr<socket_strict_mock> _pub{std::make_shared<socket_strict_mock>()};
};

TEST_F(message_handler_test, wrong_topic) {
  message_handler::message_handler handler(_pub, _rotator, _filter2m, _filter70cm);
  gcs::socket::message msg = {.topic = "deadbeef", .data = ""};
  handler.handle(msg);
}

TEST_F(message_handler_test, set_relays_with_empty_json) {
  message_handler::message_handler handler(_pub, _rotator, _filter2m, _filter70cm);
  gcs::socket::message msg = {.topic = "set_relays", .data = "{}"};
  handler.handle(msg);
}

TEST_F(message_handler_test, set_relays_with_wrong_data) {
  message_handler::message_handler handler(_pub, _rotator, _filter2m, _filter70cm);
  gcs::socket::message msg = {.topic = "set_relays", .data = "{\"gpio\":\"feedc0de\"}"};
  handler.handle(msg);
}

TEST_F(message_handler_test, set_relays_rotator_true) {
  EXPECT_CALL(*_rotator, set_value(true)).Times(1);
  message_handler::message_handler handler(_pub, _rotator, _filter2m, _filter70cm);
  gcs::socket::message msg = {.topic = "set_relays", .data = "{\"rotator\":true}"};
  handler.handle(msg);
}

TEST_F(message_handler_test, set_relays_rotator_false) {
  EXPECT_CALL(*_rotator, set_value(false)).Times(1);
  message_handler::message_handler handler(_pub, _rotator, _filter2m, _filter70cm);
  gcs::socket::message msg = {.topic = "set_relays", .data = "{\"rotator\":false}"};
  handler.handle(msg);
}

TEST_F(message_handler_test, set_relays_filter2m_true) {
  EXPECT_CALL(*_filter2m, set_value(true)).Times(1);
  message_handler::message_handler handler(_pub, _rotator, _filter2m, _filter70cm);
  gcs::socket::message msg = {.topic = "set_relays", .data = "{\"filter2m\":true}"};
  handler.handle(msg);
}

TEST_F(message_handler_test, set_relays_filter2m_false) {
  EXPECT_CALL(*_filter2m, set_value(false)).Times(1);
  message_handler::message_handler handler(_pub, _rotator, _filter2m, _filter70cm);
  gcs::socket::message msg = {.topic = "set_relays", .data = "{\"filter2m\":false}"};
  handler.handle(msg);
}

TEST_F(message_handler_test, set_relays_filter70cm_true) {
  EXPECT_CALL(*_filter70cm, set_value(true)).Times(1);
  message_handler::message_handler handler(_pub, _rotator, _filter2m, _filter70cm);
  gcs::socket::message msg = {.topic = "set_relays", .data = "{\"filter70cm\":true}"};
  handler.handle(msg);
}

TEST_F(message_handler_test, set_relays_filter70cm_false) {
  EXPECT_CALL(*_filter70cm, set_value(false)).Times(1);
  message_handler::message_handler handler(_pub, _rotator, _filter2m, _filter70cm);
  gcs::socket::message msg = {.topic = "set_relays", .data = "{\"filter70cm\":false}"};
  handler.handle(msg);
}

class relays_handlers_test : public TestWithParam<std::array<bool, 3>> {
 public:
  std::shared_ptr<gpio_strict_mock> _rotator{std::make_shared<gpio_strict_mock>()};
  std::shared_ptr<gpio_strict_mock> _filter2m{std::make_shared<gpio_strict_mock>()};
  std::shared_ptr<gpio_strict_mock> _filter70cm{std::make_shared<gpio_strict_mock>()};
  std::shared_ptr<socket_strict_mock> _pub{std::make_shared<socket_strict_mock>()};
};

// clang-format off
INSTANTIATE_TEST_SUITE_P(relays_values,
                         relays_handlers_test,
                         Values(std::array<bool, 3>{false, false,  false},
                                std::array<bool, 3>{false, false,  true},
                                std::array<bool, 3>{false, true,   false},
                                std::array<bool, 3>{false, true,   true},
                                std::array<bool, 3>{true,  false,  false},
                                std::array<bool, 3>{true,  false,  true},
                                std::array<bool, 3>{true,  true,   false},
                                std::array<bool, 3>{true,  true,   true}
                         ));

// clang-format on

TEST_P(relays_handlers_test, set_relays) {
  auto expected = GetParam();
  message_handler::message_handler handler(_pub, _rotator, _filter2m, _filter70cm);

  nlohmann::json data{};
  data["rotator"] = expected[0];
  data["filter2m"] = expected[1];
  data["filter70cm"] = expected[2];

  EXPECT_CALL(*_rotator, set_value(expected[0])).Times(1);
  EXPECT_CALL(*_filter2m, set_value(expected[1])).Times(1);
  EXPECT_CALL(*_filter70cm, set_value(expected[2])).Times(1);

  handler.handle({.topic = "set_relays", .data = data.dump()});
}

TEST_P(relays_handlers_test, get_relays) {
  auto expected = GetParam();
  message_handler::message_handler handler(_pub, _rotator, _filter2m, _filter70cm);

  nlohmann::json expected_response{};
  expected_response["rotator"] = expected[0];
  expected_response["filter2m"] = expected[1];
  expected_response["filter70cm"] = expected[2];

  EXPECT_CALL(*_rotator, get_value()).WillOnce(Return(expected[0]));
  EXPECT_CALL(*_filter2m, get_value()).WillOnce(Return(expected[1]));
  EXPECT_CALL(*_filter70cm, get_value()).WillOnce(Return(expected[2]));
  EXPECT_CALL(*_pub, send(_)).WillOnce(Invoke([expected_response](const gcs::socket::message& msg) {
    EXPECT_EQ(msg.topic, "relays_state");
    EXPECT_EQ(msg.data, expected_response.dump());
    return true;
  }));

  handler.handle({.topic = "get_relays", .data = ""});
}

}  // namespace
