#include "message_handler/message_handler.hpp"

#include <optional>

#include "gpio/gpio_interface.hpp"
#include "nlohmann/json.hpp"
#include "gcs/socket/socket_base.hpp"
#include "spdlog/spdlog.h"

namespace message_handler {

message_handler::message_handler(std::weak_ptr<gcs::socket::socket_base> pub_socket,
                                 std::weak_ptr<gpio::gpio_interface> rotator_gpio,
                                 std::weak_ptr<gpio::gpio_interface> filter2m_gpio,
                                 std::weak_ptr<gpio::gpio_interface> filter70cm_gpio)
    : _pub_socket(pub_socket),
      _rotator_gpio(rotator_gpio),
      _filter2m_gpio(filter2m_gpio),
      _filter70cm_gpio(filter70cm_gpio) {
  _handlers["set_relays"] = [this](const std::string &data) { this->handle_set_relays(data); };
  _handlers["get_relays"] = [this](const std::string &data) { this->handle_get_relays(data); };
}

void message_handler::handle(const gcs::socket::message &msg) {
  if (auto handler = _handlers.find(msg.topic); handler != _handlers.end()) {
    handler->second(msg.data);
  }
}

static void set_relay(std::weak_ptr<gpio::gpio_interface> gpio, const nlohmann::json &msg,
                      const std::string &name) {
  if (msg.contains(name)) {
    if (auto ptr = gpio.lock()) {
      try {
        auto value = msg[name].get<bool>();
        ptr->set_value(value);
        spdlog::info("{} relay set to {}", name, static_cast<std::int32_t>(value));
      } catch (const std::exception &e) {
        spdlog::error("{} relay set value error: {}", name, e.what());
      }
    } else {
      spdlog::error("{} gpio lock error", name);
    }
  } else {
    spdlog::info("Request doesn't contain {}", name);
  }
}

void message_handler::handle_set_relays(const std::string &msg) {
  auto request = nlohmann::json::parse(msg);

  set_relay(_rotator_gpio, request, "rotator");
  set_relay(_filter2m_gpio, request, "filter2m");
  set_relay(_filter70cm_gpio, request, "filter70cm");

  static_cast<void>(msg);
}

static std::optional<bool> get_relay(std::weak_ptr<gpio::gpio_interface> gpio,
                                     const std::string_view name) {
  if (auto ptr = gpio.lock()) {
    auto value = ptr->get_value();
    spdlog::info("{} relay value: {}", name, static_cast<std::int32_t>(value));
    return value;
  } else {
    spdlog::error("{} gpio lock error", name);
    return std::nullopt;
  }
}

void message_handler::handle_get_relays(const std::string &msg) {
  static_cast<void>(msg);
  nlohmann::json response{};

  auto result = get_relay(_rotator_gpio, "rotator");
  if (result.has_value()) {
    response["rotator"] = *result;
  }

  result = get_relay(_filter2m_gpio, "filter2m");
  if (result.has_value()) {
    response["filter2m"] = *result;
  }

  result = get_relay(_filter70cm_gpio, "filter70cm");
  if (result.has_value()) {
    response["filter70cm"] = *result;
  }

  if (auto socket = _pub_socket.lock()) {
    socket->send({.topic = "relays_state", .data = response.dump()});
  }
}

}  // namespace message_handler
