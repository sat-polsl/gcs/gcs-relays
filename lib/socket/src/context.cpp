#include "gcs/socket/context.hpp"

namespace gcs::socket {
zmq::context_t& context() {
  static zmq::context_t ctx;
  return ctx;
}
}  // namespace gcs::socket
