#pragma once

namespace gpio {
class gpio_interface {
 public:
  virtual bool get_value() = 0;
  virtual void set_value(bool value) = 0;

 protected:
  ~gpio_interface() = default;
};
}  // namespace gpio