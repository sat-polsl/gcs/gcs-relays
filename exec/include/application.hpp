#pragma once
#include <cstdint>
#include <memory>
#include <mutex>
#include <string>

#include "gpio/gpio.hpp"
#include "message_handler/message_handler.hpp"
#include "gcs/socket/socket.hpp"

namespace application {
class application {
 public:
  application(const application&) = delete;
  application& operator=(const application&) = delete;
  application(application&&) noexcept = delete;
  application& operator=(application&&) noexcept = delete;
  ~application() = default;

  std::int32_t run(int argc, char** argv);
  void stop();

  static void terminate_handler(int sig);

  static application& get_application();

 private:
  application() = default;

  bool parse_args(int argc, char** argv);

  bool _is_running{true};
  std::mutex _mtx;

  std::string _address;
  std::string _pub_port;
  std::string _sub_port;
  std::uint32_t _rotator_pin;
  std::uint32_t _filter2m_pin;
  std::uint32_t _filter70cm_pin;

  std::shared_ptr<gcs::socket::socket> _pub_socket;
  std::shared_ptr<gcs::socket::socket> _sub_socket;
  std::shared_ptr<gpio::gpio> _rotator_gpio;
  std::shared_ptr<gpio::gpio> _filter2m_gpio;
  std::shared_ptr<gpio::gpio> _filter70cm_gpio;
};
}  // namespace application
