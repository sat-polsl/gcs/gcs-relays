#pragma once

#include "zmq.hpp"

namespace gcs::socket {
zmq::context_t& context();
}
