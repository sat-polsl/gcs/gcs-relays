set(CXXOPTS_VERSION 4e208b95b7ae488b0d31fcc80228b141792b79b5)
set(CXXOPTS_URL https://github.com/jarro2783/cxxopts/archive/${CXXOPTS_VERSION}.zip)

include(FetchContent)

FetchContent_Declare(
    cxxopts
    URL ${CXXOPTS_URL}
)

FetchContent_GetProperties(cxxopts)
if (NOT cxxopts_POPULATED)
    FetchContent_Populate(cxxopts)
    add_subdirectory(${cxxopts_SOURCE_DIR} ${cxxopts_BINARY_DIR})
endif ()
