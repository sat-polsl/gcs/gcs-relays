# GCS Relays
## Environmental variables
Variables needed to run gcs-relays in docker

 * **PROXY_PI** - IP of proxy server
 * **PUB_PORT** - connect publisher to this port
 * **SUB_PORT** - connect subscriber to this port
 * **ROTATOR_GPIO** - number of RPi GPIO connected to relay controlling rotator power supply.
 * **FILTER2M_GPIO** - number of RPi GPIO connected to relay controlling 2 m band filter power supply.
 * **FILTER70CM_GPIO** - number of RPi GPIO connected to relay controlling 70 cm band filter power supply.

## Build and run

1. Build: `docker-compose build`
2. Run: `docker-compose up -d`
