#include "application.hpp"

#include <csignal>
#include <cstdio>

#include "cxxopts.hpp"
#include "gcs/socket/context.hpp"
#include "spdlog/spdlog.h"

namespace application {
std::int32_t application::run(int argc, char **argv) {
  if (!parse_args(argc, argv)) {
    return 0;
  }

  std::signal(SIGTERM, application::terminate_handler);

  _sub_socket = std::make_shared<gcs::socket::socket>(_address, _sub_port, zmq::socket_type::sub);
  _sub_socket->subscribe("set_relay");
  _sub_socket->subscribe("get_relay");
  _sub_socket->set_receive_timeout(std::chrono::seconds(60));
  spdlog::info("Connected subscriber to {}:{}", _address, _sub_port);

  _pub_socket = std::make_shared<gcs::socket::socket>(_address, _pub_port, zmq::socket_type::pub);
  spdlog::info("Connected publisher to {}:{}", _address, _pub_port);

  _rotator_gpio = std::make_shared<gpio::gpio>(_rotator_pin, false);
  spdlog::info("Rotator relay set on pin {}", _rotator_pin);

  _filter2m_gpio = std::make_shared<gpio::gpio>(_filter2m_pin, false);
  spdlog::info("Filter 2m set on pin {}", _filter2m_pin);

  _filter70cm_gpio = std::make_shared<gpio::gpio>(_filter70cm_pin, false);
  spdlog::info("Filter 70cm set on pin {}", _filter70cm_pin);

  spdlog::info("gcs-relays started");
  message_handler::message_handler handler(_pub_socket, _rotator_gpio, _filter2m_gpio,
                                           _filter70cm_gpio);

  while (true) {
    if (auto lck = std::unique_lock(_mtx)) {
      if (!_is_running) {
        break;
      }
    }

    auto received = _sub_socket->receive();
    if (received.has_value()) {
      spdlog::info("Received topic '{}' with data '{}'", received->topic, received->data);
      handler.handle(*received);
    }
  }

  _pub_socket->close();
  _sub_socket->close();

  return 0;
}

bool application::parse_args(int argc, char **argv) {
  cxxopts::Options opts("gcs-relays", "RPi relays controller");

  // clang-format off
  opts.add_options()
      ("h,help", "Print usage")
      ("proxy", "Proxy adress", cxxopts::value<std::string>())
      ("pub", "Publisher port", cxxopts::value<std::string>())
      ("sub", "Subscriber port", cxxopts::value<std::string>())
      ("rotator", "Rotator GPIO number", cxxopts::value<std::uint32_t>())
      ("filter2m", "RF filter 2 m band GPIO number", cxxopts::value<std::uint32_t>())
      ("filter70cm", "RF filter 70 cm band GPIO number", cxxopts::value<std::uint32_t>());
  // clang-format on

  auto result = opts.parse(argc, argv);

  if (result.count("help")) {
    std::printf("%s\n", opts.help().c_str());
    return false;
  }

  try {
    _address = result["proxy"].as<std::string>();
    _pub_port = result["pub"].as<std::string>();
    _sub_port = result["sub"].as<std::string>();
    _rotator_pin = result["rotator"].as<std::uint32_t>();
    _filter2m_pin = result["filter2m"].as<std::uint32_t>();
    _filter70cm_pin = result["filter70cm"].as<std::uint32_t>();
  } catch (const std::exception &) {
    std::printf("%s\n", opts.help().c_str());
    return false;
  }

  return true;
}

void application::stop() {
  std::lock_guard lck(_mtx);
  _is_running = false;
  gcs::socket::context().shutdown();
}

void application::terminate_handler(int sig) {
  static_cast<void>(sig);
  get_application().stop();
}

application &application::get_application() {
  static application app;
  return app;
}
}  // namespace application
